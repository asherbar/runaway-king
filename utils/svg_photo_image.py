import io
import os

try:
    import wand.image
except ImportError:
    # See https://stackoverflow.com/a/41772062/2016436
    os.environ['MAGICK_HOME'] = '/usr/local/opt/imagemagick@6'
    import wand.image

from wand.api import library
import wand.color

import PIL.Image
import PIL.ImageTk


def get_image_from_svg(svg_str):
    svg_blob = svg_str.encode('utf-8')
    with wand.image.Image() as image:
        with wand.color.Color('white') as background_color:
            library.MagickSetBackgroundColor(image.wand, background_color.resource)
        image.read(blob=svg_blob)
        png_image = image.make_blob('png32')
    image = PIL.Image.open(io.BytesIO(png_image))
    return PIL.ImageTk.PhotoImage(image)
