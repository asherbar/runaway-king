import os
import string
import sys
from typing import Tuple

from chess import Board, Piece, PIECE_NAMES

from utils.logger_utils import get_logger

logger = get_logger(__name__)

try:
    SWI_PROLOG_EXECUTABLE_LOCATION = os.environ['SWI_PROLOG_EXECUTABLE_LOCATION']
except KeyError:
    raise RuntimeError('The environment variable "SWI_PROLOG_EXECUTABLE_LOCATION" must be set to SWI executable '
                       'location before executing the script')

if sys.platform == 'darwin' and 'DYLD_FALLBACK_LIBRARY_PATH' not in os.environ:
    raise RuntimeError('The environment variable "DYLD_FALLBACK_LIBRARY_PATH" must be set to the directory that '
                       'contains libswipl.dylib before executing the script')

try:
    from pyswip import Prolog
except ValueError:
    os.environ['PATH'] += ':' + SWI_PROLOG_EXECUTABLE_LOCATION
    from pyswip import Prolog


class Ai:
    _NEXT_ROW_VAR_NAME = 'NextRow'
    _NEXT_COLUMN_VAR_NAME = 'NextColumn'
    _INDEX_TO_COLUMN_LETTER = {i: string.ascii_lowercase[i - 1] for i in range(1, 9)}

    def __init__(self, max_search_depth):
        self._max_search_depth = max_search_depth
        self._prolog = Prolog()
        dir_path = os.path.dirname(os.path.realpath(__file__))
        self._prolog.consult(os.path.join(dir_path, 'player.pl'))

    def get_black_king_next_move(self, current_board: Board):
        prolog_board = self._convert_to_prolog_board(current_board)
        query = 'next_black_king_move(black/{}, {}, {}/{})'.format(prolog_board, self._max_search_depth,
                                                                   self._NEXT_ROW_VAR_NAME,
                                                                   self._NEXT_COLUMN_VAR_NAME)
        logger.info('Sending query "{}" to Prolog'.format(query))
        query_res_list = list(self._prolog.query(query))
        if len(query_res_list) > 1:
            logger.warning('Expected at most 1 query result. Instead got {}'.format(query_res_list))
        if not query_res_list:
            logger.info('No results found. Checkmate!')
            return
        query_res = query_res_list[0]
        return query_res['NextRow'], query_res['NextColumn']

    @classmethod
    def _convert_to_prolog_board(cls, python_board: Board):
        piece_map = python_board.piece_map()
        return cls._convert_to_prolog_list([cls._convert_to_prolog_piece(item) for item in piece_map.items()])

    @classmethod
    def _convert_to_prolog_piece(cls, piece_map_item: Tuple[int, Piece]):
        index, piece = piece_map_item
        color = 'white' if piece.symbol().isupper() else 'black'
        algebraic_notation_row, algebraic_notation_column = cls._calculate_algebraic_from_index(index)
        prolog_type = PIECE_NAMES[piece.piece_type]
        return '/'.join((color, prolog_type, str(algebraic_notation_row), algebraic_notation_column))

    @classmethod
    def _calculate_algebraic_from_index(cls, index):
        row = (index // 8) + 1
        column = cls._INDEX_TO_COLUMN_LETTER[(index % 8) + 1]
        return row, column

    @classmethod
    def _convert_to_prolog_list(cls, l):
        return '[{}]'.format(','.join(l))
