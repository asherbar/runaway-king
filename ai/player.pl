:- use_module(alpha_beta_module).
:- use_module(library(lists)).

%%%%% Board %%%%%
% A board is defined as a list of pieces:
%   [Piece1, Piece2, ..., PieceN]

%%%%% Piece %%%%%
% Each Piece is defined with the following structure:
%   Color/Type/Row/Column where:
% Color is either 'black' or 'white'
% Type is one of 'king', 'queen', 'rook', 'bishop', 'knight', 'pawn'
% Row is one of '1', '2', '3', '4', '5', '6', '7', '8'
% Column is one of 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'.
% For example: white/queen/1/a describes a white queen positioned in the bottom left square

%%%%% KingsMove %%%%%
% A KingsMove is defined as:
%   Row/Column
% Where Row is the target move's row and Column is the target's move column.
% E.g. the move 2/b means that the black king will move to row 2 column b

%%%%% State %%%%%
% A state is defined as:
%   ColorTurn/Board
% Where ColorTurn is the color of the player who's turn it is and Board is the current board positions

%%%%% Main predicate %%%%%
% "Returns" the next move of the black king
next_black_king_move(black/CurrentBoard, MaxDepth, NextRow/NextColumn):-
    % Calculate next board:
    alpha_beta(black/CurrentBoard, MaxDepth, white/NextBoard),
    % Find the king's position in the next board:
    member(black/king/NextRow/NextColumn, NextBoard).

%%%%% Required predicates for Alpha-Beta %%%%%
moves(ColorTurn/CurrentBoard, PossibleNextStatesList):-
    findall(NextState, next_board(ColorTurn/CurrentBoard, NextState), PossibleNextStatesList).

% The heuristic that will be used: the number of possible moves for the black king
static_val(_/CurrentBoard, Val):-
    member(black/king/BlackKingRow/BlackKingColumn, CurrentBoard),
    findall(PossibleMove, possible_piece_move(CurrentBoard, black/king/BlackKingRow/BlackKingColumn, PossibleMove), PossibleMoves),
    length(PossibleMoves, Val).

min_to_move(white/_).
max_to_move(black/_).
    
%%%%% Auxiliary predicates %%%%%

% next_board(CurrentState, NextState) is true if you can get to NextState from CurrentState in a single move
next_board(ColorTurn/CurrentBoard, OppositeColor/NextBoard):-
    opposite_color(ColorTurn, OppositeColor),
    possible_color_move(CurrentBoard, ColorTurn, Piece, Move),
    % NextBoard is the current board after given piece's move:
    move_piece_on_board(CurrentBoard, Piece, Move, NextBoard).

% possible_color_move(Board, Color, Piece, Move) is true if a Piece with Color can Move given Board
possible_color_move(Board, Color, Color/Type/Row/Column, Move):-
    % Get a piece in the current board of the given color:
    member(Color/Type/Row/Column, Board),   
     % Get a possible move of given piece:
    possible_piece_move(Board, Color/Type/Row/Column, Move).

% not_member(Elem, List) is true if Elem is not a member in List
not_member(_, []) :- !.
not_member(X, [Head|Tail]) :-
     X \= Head,
    not_member(X, Tail).

opposite_color(white, black).
opposite_color(black, white).

%%%%% Knowledge base predicates %%%%%

%%% Column and Row calculators %%%

plus_row(X, Y, Z):-
    Z is X + Y,
    check_row_bounds(Z).

plus_column(X, Y, Z):-
    name(X, [XCode]),
    ZCode is XCode + Y,
    check_column_bounds(ZCode),
    name(Z, [ZCode]).

% check_row_bounds(X) is true if X is a row index that's within the board's limit
check_row_bounds(X):-
    X >= 1, 
    X =< 8.

% check_column_bounds(XAsciiCode) is true if XAsciiCode is an ascii code of a column index that's within the board's limit
check_column_bounds(XAsciiCode):-
    % Ascii of 'a' is 97
    XAsciiCode >= 97, 
    % Ascii of 'h' is 104
    XAsciiCode =< 104.

%%% Piece logic %%%

% possible_piece_move(Board, Piece, Move) is true if Piece can move to MoveRow/MoveColumn given Board

%% Pawn movement %%
% Pawns are a special case as they move differently depending on enemy pieces.
% White pawn regular move: one step forward
possible_piece_move(Board, white/pawn/Row/Column, MoveRow/Column):-
    plus_row(Row, 1, MoveRow),
    not_member(_/_/MoveRow/Column, Board).
% White pawn attack move: one step diagonally forward
possible_piece_move(Board, white/pawn/Row/Column, MoveRow/MoveColumn):-
    threatening_area(Board, white/pawn/Row/Column, MoveRow/MoveColumn),
    member(black/_/MoveRow/MoveColumn, Board).
% Black pawn regular move: one step backward
possible_piece_move(Board, black/pawn/Row/Column, MoveRow/Column):-
    plus_row(Row, -1, MoveRow),
    not_member(_/_/MoveRow/Column, Board).
% Black pawn attack move: one step diagonally backward
possible_piece_move(Board, black/pawn/Row/Column, MoveRow/MoveColumn):-
    threatening_area(Board, black/pawn/Row/Column, MoveRow/MoveColumn),
    member(white/_/MoveRow/MoveColumn, Board).

%% Knight movement %%
possible_piece_move(Board, Color/knight/Row/Column, MoveRow/MoveColumn):-
    threatening_area(Board, Color/knight/Row/Column, MoveRow/MoveColumn),
    not_member(Color/_/MoveRow/MoveColumn, Board).

%% Bishop movement %%
possible_piece_move(Board, Color/bishop/Row/Column, MoveRow/MoveColumn):-
    threatening_area(Board, Color/bishop/Row/Column, MoveRow/MoveColumn),
    not_member(Color/_/MoveRow/MoveColumn, Board).

%% Rook movement %%
possible_piece_move(Board, Color/rook/Row/Column, MoveRow/MoveColumn):-
    threatening_area(Board, Color/rook/Row/Column, MoveRow/MoveColumn),
    not_member(Color/_/MoveRow/MoveColumn, Board).

%% Queen movement %%
possible_piece_move(Board, Color/queen/Row/Column, MoveRow/MoveColumn):-
    possible_piece_move(Board, Color/bishop/Row/Column, MoveRow/MoveColumn);
    possible_piece_move(Board, Color/rook/Row/Column, MoveRow/MoveColumn).

%% King movement %%
possible_piece_move(Board, Color/king/Row/Column, MoveRow/MoveColumn):-
    immediate_positions_around(Row/Column, MoveRow/MoveColumn),
    not_member(Color/_/MoveRow/MoveColumn, Board),
    move_piece_on_board(Board, Color/king/Row/Column, MoveRow/MoveColumn, TempBoard),
    opposite_color(Color, OppositeColor),
    findall(ThreatenedPosition, threatened_position(TempBoard, OppositeColor, ThreatenedPosition), ThreatenedPositions),
    not_member(MoveRow/MoveColumn, ThreatenedPositions).

% threatening_area(Board, Piece, Move) is true if Move is threatened (or defended) by Piece, given Board 
threatening_area(_, black/pawn/Row/Column, MoveRow/MoveColumn):-
    plus_row(Row, -1, MoveRow),
    (plus_column(Column, 1, MoveColumn) ; plus_column(Column, -1, MoveColumn)).

threatening_area(_, white/pawn/Row/Column, MoveRow/MoveColumn):-
    plus_row(Row, 1, MoveRow),
    (plus_column(Column, 1, MoveColumn) ; plus_column(Column, -1, MoveColumn)).

threatening_area(_, _/knight/Row/Column, MoveRow/MoveColumn):-
    (plus_row(Row, 2, MoveRow) ; plus_row(Row, -2, MoveRow)),
    (plus_column(Column, 1, MoveColumn) ; plus_column(Column, -1, MoveColumn)).
threatening_area(_, _/knight/Row/Column, MoveRow/MoveColumn):-
    (plus_row(Row, 1, MoveRow) ; plus_row(Row, -1, MoveRow)),
    (plus_column(Column, 2, MoveColumn) ; plus_column(Column, -2, MoveColumn)).

threatening_area(Board, _/rook/Row/Column, MoveRow/MoveColumn):-
    north_vertical(Board, Row/Column, MoveRow/MoveColumn);
    west_horizontal(Board, Row/Column, MoveRow/MoveColumn);
    south_vertical(Board, Row/Column, MoveRow/MoveColumn);
    east_horizontal(Board, Row/Column, MoveRow/MoveColumn).

threatening_area(Board, _/bishop/Row/Column, MoveRow/MoveColumn):-
    north_east_diagonal(Board, Row/Column, MoveRow/MoveColumn);
    north_west_diagonal(Board, Row/Column, MoveRow/MoveColumn);
    south_east_diagonal(Board, Row/Column, MoveRow/MoveColumn);
    south_west_diagonal(Board, Row/Column, MoveRow/MoveColumn).

threatening_area(Board, Color/queen/Row/Column, MoveRow/MoveColumn):-
    threatening_area(Board, Color/rook/Row/Column, MoveRow/MoveColumn);
    threatening_area(Board, Color/bishop/Row/Column, MoveRow/MoveColumn).

% immediate_positions_around(FromPos, ToPos) is true if ToPos is a position immediately around FromPos
immediate_positions_around(FromRow/FromColumn, ToRow/ToColumn):-
    member(X, [-1, 0, 1]),
    member(Y, [-1, 0, 1]),
    [X, Y] \= [0, 0],
    plus_row(FromRow, X, ToRow),
    plus_column(FromColumn, Y, ToColumn).

% threatened_position(Board, Color, ThreatenedPosition) is true if ThreatenedPosition is a position
% (in the form of Row/Column) in Board that is threatened by a piece with Color.
threatened_position(Board, Color, ThreatenedPosition):-
    % Get a piece in the current board of the given color:
    member(Color/Type/Row/Column, Board), 
    % Differentiate between the king and the rest to avoid infinite recursion
    Type \= king,  
    % Get a possible move of given piece:
    threatening_area(Board, Color/Type/Row/Column, ThreatenedPosition).
threatened_position(Board, Color, MoveRow/MoveColumn):-
    member(Color/king/KingRow/KingColumn, Board),
    immediate_positions_around(KingRow/KingColumn, MoveRow/MoveColumn).

north_vertical(Board, FromRow/FromColumn, ToRow/FromColumn):-
    member(Dist, [1, 2, 3, 4, 5, 6, 7]),
    plus_row(FromRow, Dist, ToRow),
    % If there's a piece in this direction then that's the last result of this direction  
    ((member(_/_/ToRow/FromColumn, Board), !) ; true).

south_vertical(Board, FromRow/FromColumn, ToRow/FromColumn):-
    member(Dist, [1, 2, 3, 4, 5, 6, 7]),
    plus_row(FromRow, -Dist, ToRow),
    % If there's a piece in this direction then that's the last result of this direction  
    ((member(_/_/ToRow/FromColumn, Board), !) ; true).

west_horizontal(Board, FromRow/FromColumn, FromRow/ToColumn):-
    member(Dist, [1, 2, 3, 4, 5, 6, 7]),
    plus_column(FromColumn, Dist, ToColumn),
    % If there's a piece in this direction then that's the last result of this direction  
    ((member(_/_/FromRow/ToColumn, Board), !) ; true).

east_horizontal(Board, FromRow/FromColumn, FromRow/ToColumn):-
    member(Dist, [1, 2, 3, 4, 5, 6, 7]),
    plus_column(FromColumn, -Dist, ToColumn),
    % If there's a piece in this direction then that's the last result of this direction  
    ((member(_/_/FromRow/ToColumn, Board), !) ; true).

% x_y_diagonal(Board, FromRow/FromColumn, ToRow/ToColumn) is true if ToRow/ToColumn is in the xy direction from FromRow/FromColumn
north_east_diagonal(Board, FromRow/FromColumn, ToRow/ToColumn):-
    member(Dist, [1, 2, 3, 4, 5, 6, 7]),
    plus_row(FromRow, Dist, ToRow), 
    plus_column(FromColumn, Dist, ToColumn),
    % If there's a piece in this direction then that's the last result of this direction  
    ((member(_/_/ToRow/ToColumn, Board), !) ; true).

north_west_diagonal(Board, FromRow/FromColumn, ToRow/ToColumn):-
    member(Dist, [1, 2, 3, 4, 5, 6, 7]),
    plus_row(FromRow, Dist, ToRow), 
    plus_column(FromColumn, -Dist, ToColumn),
    % If there's a piece in this direction then that's the last result of this direction  
    ((member(_/_/ToRow/ToColumn, Board), !) ; true).

south_east_diagonal(Board, FromRow/FromColumn, ToRow/ToColumn):-
    member(Dist, [1, 2, 3, 4, 5, 6, 7]),
    plus_row(FromRow, -Dist, ToRow), 
    plus_column(FromColumn, Dist, ToColumn),
    % If there's a piece in this direction then that's the last result of this direction  
    ((member(_/_/ToRow/ToColumn, Board), !) ; true).

south_west_diagonal(Board, FromRow/FromColumn, ToRow/ToColumn):-
    member(Dist, [1, 2, 3, 4, 5, 6, 7]),
    plus_row(FromRow, -Dist, ToRow), 
    plus_column(FromColumn, -Dist, ToColumn),
    % If there's a piece in this direction then that's the last result of this direction 
    ((member(_/_/ToRow/ToColumn, Board), !) ; true).


% remove_piece(Board, Piece, BoardRes) is true if after removing Piece from Board the result board is BoardRes
% Note: No chess rules are checked in this predicate
% Note: This predicate fails if Piece isn't on Board
remove_piece(Board, Piece, BoardRes):-
    select(Piece, Board, BoardRes).

% add_piece(Board, Piece, BoardRes) is true if after adding Piece to Board the result board is BoardRes
% Note: No chess rules are checked in this predicate
add_piece(Board, Piece, BoardRes):-
    append(Board, [Piece], BoardRes).

% move_piece_on_board(Board, Piece, Move, BoardRes) is true if moving Piece to Move on Board results in BoardRes
% Note: No chess rules are checked in this predicate
% Note: If the target position contains any other piece it is removed from the board
move_piece_on_board(Board, Color/Type/FromRow/FromColumn, ToRow/ToColumn, BoardRes):-
    % Remove any piece that's currently in the target position
    remove_piece(Board, _/_/ToRow/ToColumn, TempBoard1), !,
    % Remove current piece from its current position
    remove_piece(TempBoard1, Color/Type/FromRow/FromColumn, TempBoard2),
    % Add the piece to the target position
    add_piece(TempBoard2, Color/Type/ToRow/ToColumn, BoardRes).

move_piece_on_board(Board, Color/Type/FromRow/FromColumn, ToRow/ToColumn, BoardRes):-
    % Remove current piece position
    remove_piece(Board, Color/Type/FromRow/FromColumn, TempBoard),
    % Remove current piece position
    add_piece(TempBoard, Color/Type/ToRow/ToColumn, BoardRes).
