% Implementation of the Alpha-Beta algorithm (based on implementation on page 585).

% Requires the following predicates in order to work:
%   1. moves(CurrentState, NextStateList)- is true when NextStateList a list of possible states from current state CurrentState.
%   2. static_val(State, Val)- is true when Val is the static value (possibly a heuristic value) of state State
%   3. min_to_move(State)- is true if it's the Min's player turn
%   4. max_to_move(State)- is true if it's the Max's player turn

:- module(alpha_beta_module, [alpha_beta/3]).

alpha_beta(State, MaxDepth, NextState):-
    MaxDepth >= 0,
    (alpha_beta(State, MaxDepth, -inf, inf, NextState, _), !)
    % If current depth doesn't work, try a lower one:
    ;
    (NextDepth is MaxDepth - 1, alpha_beta(State, NextDepth, NextState)).

alpha_beta(State, MaxDepth, Alpha, Beta, GoodState, Val) :-
    moves(State, StateList), MaxDepth > 0, !,
    bounded_best(StateList, MaxDepth, Alpha, Beta, GoodState, Val);
    static_val(State, Val).
bounded_best([State|StateList], MaxDepth, Alpha, Beta, GoodState, GoodVal) :-
    NewMaxDepth is MaxDepth - 1,
    alpha_beta(State, NewMaxDepth, Alpha, Beta, _, Val),
    good_enough(StateList, Alpha, Beta, State, Val, GoodState, GoodVal, MaxDepth).
good_enough([], _, _, State, Val, State, Val, _) :- !. % No other candidate
good_enough(_, Alpha, Beta, State, Val, State, Val, _) :-
    min_to_move(State), Val > Beta, !; % Maximizer attained upper bound
    max_to_move(State), Val < Alpha, !. % Minimizer attained lower bound
good_enough(StateList, Alpha, Beta, State, Val, GoodState, GoodVal, MaxDepth) :-
    new_bounds(Alpha, Beta, State, Val, NewAlpha, NewBeta), % Refine bounds
    bounded_best(StateList, MaxDepth, NewAlpha, NewBeta, State1, Val1),
    better_of(State, Val, State1, Val1, GoodState, GoodVal).
new_bounds(Alpha, Beta, State, Val, Val, Beta) :-
    min_to_move(State), Val > Alpha, !. % Maximizer increased lower bound
new_bounds(Alpha, Beta, State, Val, Alpha, Val) :-
    max_to_move(State), Val < Beta, !. % Minimizer decreased upper bound
new_bounds(Alpha, Beta, _, _, Alpha, Beta).
better_of(State, Val, _, Val1, State, Val) :-
    min_to_move(State), Val > Val1, !;
    max_to_move(State), Val < Val1, !.
better_of(_, _, State1, Val1, State1, Val1).
