import unittest

from chess import Piece, PAWN, WHITE, Board

from ai.ai import Ai


class AiTest(unittest.TestCase):
    _KINGS_ONLY_FEN = '4k3/8/8/8/8/8/8/4K3 w KQkq - 0 1'
    _BEST_TO_ATTACK_ROOK = '1k6/R7/8/8/8/8/8/4K3 w - - 0 1'
    _SURROUNDED_KING = '5R2/8/3k4/8/1R3B2/8/8/4K3 w - - 0 1'
    _ONLY_ONE_PLACE_TO_GO = '4kR2/6P1/8/8/3R4/8/8/4K3 w - - 0 1'
    _HORSE_AND_QUEEN = '4k3/2Q3N1/8/8/8/8/8/4K3 w - - 0 1'
    _KING_AND_QUEEN = '6k1/4Q3/8/5K2/8/8/8/8 w - - 0 1'
    _KING_AND_ROOK = '8/8/8/8/1RK5/k7/8/8 w - - 0 1'
    _KING_BISHOP_AND_QUEEN = '8/8/3k4/8/3Q4/3BK3/8/8 w - - 0 1'

    def test__calculate_algebraic_from_index(self):
        row, column = Ai._calculate_algebraic_from_index(0)
        self.assertEqual(row, 1)
        self.assertEqual(column, 'a')

        row, column = Ai._calculate_algebraic_from_index(63)
        self.assertEqual(row, 8)
        self.assertEqual(column, 'h')

        row, column = Ai._calculate_algebraic_from_index(14)
        self.assertEqual(row, 2)
        self.assertEqual(column, 'g')

    def test__convert_to_prolog_piece(self):
        prolog_piece = Ai._convert_to_prolog_piece((17, Piece(PAWN, WHITE)))
        self.assertEqual(prolog_piece, 'white/pawn/3/b')

    def test__convert_to_prolog_board(self):
        prolog_board = Ai._convert_to_prolog_board(Board(self._KINGS_ONLY_FEN))
        self.assertEqual(prolog_board, '[black/king/8/e,white/king/1/e]')

    def test_get_black_king_next_move_simple(self):
        ai = Ai(3)
        row, column = ai.get_black_king_next_move(Board(self._KINGS_ONLY_FEN))
        self.assertEqual(row, 8)
        self.assertEqual(column, 'f')

    def test_get_black_king_next_move_medium(self):
        ai = Ai(3)
        row, column = ai.get_black_king_next_move(Board(self._BEST_TO_ATTACK_ROOK))
        self.assertEqual(row, 7)
        self.assertEqual(column, 'a')

    def test_get_black_king_next_move_hard(self):
        ai = Ai(3)
        row, column = ai.get_black_king_next_move(Board(self._SURROUNDED_KING))
        self.assertEqual(row, 5)
        self.assertEqual(column, 'c')

    def test_get_black_king_next_move_very_hard(self):
        ai = Ai(3)
        row, column = ai.get_black_king_next_move(Board(self._ONLY_ONE_PLACE_TO_GO))
        self.assertEqual(row, 7)
        self.assertEqual(column, 'e')

    def test_get_black_king_next_move_horse_and_queen(self):
        ai = Ai(3)
        row, column = ai.get_black_king_next_move(Board(self._HORSE_AND_QUEEN))
        self.assertEqual(row, 8)
        self.assertEqual(column, 'f')

    def test_get_black_king_and_queen(self):
        ai = Ai(3)
        row, column = ai.get_black_king_next_move(Board(self._KING_AND_QUEEN))
        self.assertEqual(row, 8)
        self.assertEqual(column, 'h')

    def test_get_black_king_and_rook(self):
        ai = Ai(3)
        row, column = ai.get_black_king_next_move(Board(self._KING_AND_ROOK))
        self.assertEqual(row, 2)
        self.assertEqual(column, 'a')

    def test_get_black_king_bishop_and_queen(self):
        ai = Ai(3)
        row, column = ai.get_black_king_next_move(Board(self._KING_BISHOP_AND_QUEEN))
        self.assertEqual(row, 7)
        self.assertEqual(column, 'e')
