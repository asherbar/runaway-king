### General
My project for course 20596 in the Open university, Israel
(שפת פרולוג והיבטים לבינה מלאכותית - 20596 )
### Programmer  
Asher Bar-Eitan  
### File Name
main.py  
### Description
A game for practicing endgame checkmate of a lone king. The human (White) player aims to checkmate the computer (Black). 
This is a sub-chess game where the (human) player aims to checkmate the other player's (computer) lone King. The game starts with an initial chess board at a certain state, with the white turn to go.
### Input
N/A
### Output
N/A
### Synopsis
#### Requirements:
* Python 3.6+
* [pip](https://pypi.org/project/pip/)
* SWI-prolog 7.2.x and higher
#### Installation
Note: It's recommended to install within [virtual environment (a.k.a. "venv")](https://docs.python.org/3/library/venv.html)  
From the root of this project, run `pip3 install -r requirements.txt`. This will download and install the required Python packages for this project to run.
#### Running the game
1. Set the environment variable `SWI_PROLOG_EXECUTABLE_LOCATION` to the directory where SWI-Prolog's executable is. E.g., on MacOS this would be done by executing:  
`export SWI_PROLOG_EXECUTABLE_LOCATION='/Applications/SWI-Prolog.app/Contents/swipl/bin/x86_64-darwin15.6.0'` 
1. If running on _MacOS_: Set the environment variable `DYLD_FALLBACK_LIBRARY_PATH` to the directory that contains `libswipl.dylib`. E.g.:  
    `export DYLD_FALLBACK_LIBRARY_PATH=/Applications/SWI-Prolog.app/Contents/swipl/lib/x86_64-darwin15.6.0`  
1. From the root of the project execute: `python3 main.py`
