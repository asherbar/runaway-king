import json
import os
import tkinter as tk
import webbrowser
from tkinter import DISABLED
from tkinter import font

import PIL.Image
import PIL.ImageTk
import chess
import chess.svg

from ai.ai import Ai
from utils.logger_utils import get_logger
from utils.svg_photo_image import get_image_from_svg

_logger = get_logger(__name__)


class Application(tk.Tk):
    _TITLE = 'Runaway King'

    def __init__(self):
        super().__init__()
        self._frame = None
        self.title(self._TITLE)
        self.switch_frame(_StartPage)

    def switch_frame(self, frame_class, *args):
        """Destroys current frame and replaces it with a new one."""
        new_frame = frame_class(self, *args)
        if self._frame is not None:
            self._frame.destroy()
        self._frame = new_frame
        self._frame.pack()


class _StartPage(tk.Frame):
    _BOARDS_ROOT_PATH = 'resources/boards'
    _IMAGE_FILE_NAME = 'image.png'
    _SPEC_FILE_NAME = 'spec.json'
    _EXPERTISE_SELECTION_LABEL_ROW = 0
    _EXPERTISE_SELECTION_ROW = _EXPERTISE_SELECTION_LABEL_ROW + 1
    _BOARD_SELECTION_ROW = _EXPERTISE_SELECTION_ROW + 1
    _BOARD_ROW = _BOARD_SELECTION_ROW + 1
    _MAXIMUM_COLUMNS = 3

    _expertise_levels = {
        'Easy': 2,
        'Medium': 3,
        'Hard': 4
    }

    def __init__(self, master, *_):
        super().__init__(master)
        self._font = ('Courier', 24)
        self._expertise_selection = tk.IntVar()
        self._expertise_selection.set(self._expertise_levels['Medium'])
        self._add_expertise_selection()
        self._add_choose_board_caption()
        self._add_possible_board_options()
        self._selected_board_fen = None

    def _add_possible_board_options(self):
        board_num = 0
        for item in os.listdir(self._BOARDS_ROOT_PATH):
            board_dir_path = os.path.join(self._BOARDS_ROOT_PATH, item)
            if os.path.isdir(board_dir_path):
                self._add_possible_board_option(board_dir_path, board_num)
                board_num += 1

    def _add_possible_board_option(self, path, board_num):
        board_image_path = os.path.join(path, self._IMAGE_FILE_NAME)
        board_spec_path = os.path.join(path, self._SPEC_FILE_NAME)
        with open(board_spec_path) as f:
            board_spec_contents = json.load(f)

        pil_image = PIL.Image.open(board_image_path)

        board_image = PIL.ImageTk.PhotoImage(pil_image.resize((200, 200), PIL.Image.ANTIALIAS))

        def on_image_click_command():
            self.master.switch_frame(_GamePage, board_spec_contents['fen'], board_spec_contents['min_moves'],
                                     self._expertise_selection.get())

        board_option_label = tk.Button(self, image=board_image, command=on_image_click_command)
        row = self._BOARD_ROW + board_num // self._MAXIMUM_COLUMNS
        column = board_num % self._MAXIMUM_COLUMNS
        board_option_label.grid(row=row, column=column)
        board_option_label.image = board_image

    def _add_choose_board_caption(self):
        tk.Label(self, text='Choose a board to play:', font=self._font).grid(row=self._BOARD_SELECTION_ROW,
                                                                             columnspan=self._MAXIMUM_COLUMNS)

    def _add_expertise_selection(self):
        tk.Label(self, text='Choose the expertise level of the opponent:', font=self._font).grid(
            row=self._EXPERTISE_SELECTION_LABEL_ROW, column=0, columnspan=self._MAXIMUM_COLUMNS)
        column = 0
        for level_text, level_value in self._expertise_levels.items():
            tk.Radiobutton(self, text=level_text, variable=self._expertise_selection, value=level_value).grid(
                row=self._EXPERTISE_SELECTION_ROW, column=column)
            column += 1


class _GamePage(tk.Frame):
    _TITLE_ROW = 0
    _MOVE_CONTROL_ROW = _TITLE_ROW + 1
    _STATUS_ROW = _MOVE_CONTROL_ROW + 1
    _BOARD_ROW = _STATUS_ROW + 1

    _MOVE_INSTRUCTIONS_ADDRESS = r'https://en.wikipedia.org/wiki/Algebraic_notation_(chess)'

    def __init__(self, master, board, min_moves, expertise_level, *_):
        super().__init__(master)
        self._board = chess.Board(board)
        self._min_moves = min_moves
        self._ai = Ai(expertise_level)
        self._build_gui()

    def _build_gui(self):
        self._add_back_button()
        self._add_perfect_game_moves()
        self._add_message_label()
        self._add_move_widgets()
        self._add_board_method()
        self._refresh_board()

    def _add_board_method(self):
        self._board_label = tk.Label(self)
        self._board_label.grid(row=self._BOARD_ROW, columnspan=3)

    def _add_move_widgets(self):
        move_label = tk.Label(self, text='Enter your move:', fg='blue', cursor='hand2')
        label_font = font.Font(move_label, move_label.cget('font'))
        label_font.config(underline=True)
        move_label.config(font=label_font)
        move_label.grid(row=self._MOVE_CONTROL_ROW, sticky=tk.W)
        move_label.bind('<Button-1>', lambda e: webbrowser.open_new(self._MOVE_INSTRUCTIONS_ADDRESS))
        self._player_move_entry = tk.Entry(self)
        self._player_move_entry.focus()
        self._player_move_entry.grid(row=self._MOVE_CONTROL_ROW, column=1)
        self._player_move_entry.bind('<Return>', self._on_move_button_click)
        self._move_button = tk.Button(self, text='Move', command=self._on_move_button_click)
        self._move_button.grid(row=self._MOVE_CONTROL_ROW, column=2, sticky=tk.E)

    def _add_message_label(self):
        self._message_label = tk.Label(self)
        self._message_label.grid(row=self._STATUS_ROW)

    def _add_back_button(self):
        tk.Button(self, text='⇦', command=self._on_back_button_click).grid(row=self._TITLE_ROW, column=0)

    def _add_perfect_game_moves(self):
        tk.Label(self, text='Mate in {} if played perfectly'.format(self._min_moves), font=('Courier', 14)).grid(
            row=self._TITLE_ROW, column=1)

    def _on_move_button_click(self, *_):
        player_entry = self._player_move_entry.get()
        try:
            self._board.push_san(player_entry)
        except ValueError:
            self._message_label.config(text='Illegal move: {}'.format(player_entry))
            return
        self._refresh_board()
        if not self._check_if_game_ended():
            self._do_computers_move()

    def _on_back_button_click(self):
        self.master.switch_frame(_StartPage)

    def _refresh_board(self):
        image = get_image_from_svg(chess.svg.board(self._board))
        self._board_label.config(image=image)
        # Must keep reference to image: https://stackoverflow.com/a/15216402/2016436
        self._board_label.image = image

    def _check_if_game_ended(self):
        game_ended = False
        if self._board.is_checkmate():
            self._message_label.config(text='You win!')
            game_ended = True
        elif self._board.is_stalemate():
            self._message_label.config(text='Stalemate!')
            game_ended = True
        if game_ended:
            self._end_game()
        return game_ended

    def _end_game(self):
        self._move_button.config(state=DISABLED)

    def _do_computers_move(self):
        row, column = self._ai.get_black_king_next_move(self._board)
        self._board.push_san('K{}{}'.format(column, row))
        self._refresh_board()
